CREATE TABLE `sensorData` (
  `buttonB_is` tinyint(1) NOT NULL,
  `buttonB_was` tinyint(1) NOT NULL,
  `buttonA_is` tinyint(1) NOT NULL,
  `buttonA_was` tinyint(1) NOT NULL,
  `gesture` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `z` float NOT NULL,
  `temp` float NOT NULL,
  `heading` int NOT NULL,
  `fieldStrength` int NOT NULL,
  `runningTime` int NOT NULL,
  `time` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
