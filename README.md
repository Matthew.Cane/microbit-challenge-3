# Microbit Challenge 3: Node-RED Dashboard and API for Micro:Bit Sensors

## Overview

This system provides functionality that allows a user to view live sensor data from a Micro:Bit though a web interface and a web API as well as save the data to a MySQL database.

The system flow is as follows:

1.  Sensor data is collected in the Micro:Bit and packaged as valid JSON data.
2.  The JSON data is sent via USB serial to a computer.
3.  The serial data is read by a Node-RED application which then parses and formats the data.
4.  The Node-RED application creates an HTTP end-point and presents the JSON as an API.
5.  The Node-RED application generates a web interface that presents the data in an easy to read manner.
6.  The Node-RED application generates an SQL INSERT statement that will save the sensor data to a MySQL database.

The Node-RED application is running in a Docker container, however it can be run locally.
The Micro:Bit sends a new set of sensor readings every 500ms. It can be made to refresh faster by adjusting the `sleep()` function call in [challenge3.py](challenge3.py), however the dashboard can become unstable with faster refresh times.

Below is a screenshot of the Node-RED application flow:
![Node-Red application flow](Node-RED-Flow.png)

Below is a screenshot of the web dashboard:
![Node-Red Dashboard](dashboard.png)



## Video Demo

Click here to view demo video:

[![Demo Video](Title-Card.png)](http://www.youtube.com/watch?v=3_VD7RtUTC8)

A copy of this video can be found in this repository.

## Setup

1.  The Micro:Bit must be connected to a computer via USB and flashed with the [challenge3.py](challenge3.py) file. This can be done using the uFlash utility that can be found [here.](https://uflash.readthedocs.io/en/latest/)
2. A Node-RED environment needs to be set up. Installation instructions can be found [here.](https://nodered.org/docs/getting-started)
4. A MySQL Database must be set up with a table called `sensorData`. The hostname, user, password and database can be configured in the mysql node in the Node-RED application. An example CREATE TABLE statement can be found [here.](createTable.sql) Note, due to the limitations of the mysql Node-RED plugin, the database user must be using the `native MySQL authentication` authentication plugin.
3.  Once the Node-RED environment is set up, either locally or with Docker, three additional pallets must be installed: [node-red-node-serialport](https://flows.nodered.org/node/node-red-node-serialport), [node-red-dashboard](https://flows.nodered.org/node/node-red-dashboard) and [node-red-node-mysql](https://flows.nodered.org/node/node-red-node-mysql). These can be installed by going to `Hamburger Button > Manage Palletes > Install`
4.  Import the Node-RED flow by copying the contents of [nodeRED.json](nodeRED.json) and navigating to `Hamburger Button > Import`
5.  Once the flow has been imported, click the `Deploy` button.
6.  The `uBit serial` node should then show a connection has been made to the Micro:Bit.

## Usage

*  Browsing to [http://localhost:1880/ui](http://localhost:1880/ui) will display the dashboard. It may take several seconds for the dashboard to populate with data.
*  Browsing to [http://localhost:1880/api](http://localhost:1880/api) will return a JSON string containing the raw sensor data from the Micro:Bit.

Below is an example of the JSON text data returned by the API:

```
{
	"buttonB_is":false,
	"buttonB_was":false,
	"buttonA_was":false,
	"gesture":"face up",
	"y":136,
	"x":4,
	"temp":28,
	"buttonA_is":true,
	"z":-1012,
	"heading":261,
	"fieldStrength":35028,
	"runningTime":3414890,
	"time":1584274428103
}
```
