from microbit import *
uart.init(baudrate=115200)

while True:
    data = dict()
    data["temp"] = str(temperature())
    data["gesture"] = str(accelerometer.current_gesture())
    data["buttonA_is"] = str(button_a.is_pressed())
    data["buttonB_is"] = str(button_b.is_pressed())
    data["buttonA_was"] = str(button_a.was_pressed())
    data["buttonB_was"] = str(button_b.was_pressed())
    data["x"] = str(accelerometer.get_x())
    data["y"] = str(accelerometer.get_y())
    data["z"] = str(accelerometer.get_z())
    data["runningTime"] = str(running_time())
    data["heading"] = str(compass.heading())
    data["fieldStrength"] = str(compass.get_field_strength())

    data = str(data).replace("'", '"')

    uart.write(data+"\n\r")
    sleep(500)
